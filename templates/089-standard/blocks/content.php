<?php
/**
 * @author   	089webdesgin.de
 * @copyright   Copyright (C) 2015 089webdesgin.de. All rights reserved.
 * @URL 		https://089webdesgin.de/
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

$span = 9;
if ($this->countModules('left') && $this->countModules('right')) $span = 6;
if (!$this->countModules('left') && !$this->countModules('right')) $span = 12;

?>
<div class="content content--wrapper">
	<div class="fullwidth ifTeam">
		<?php if($this->countModules('headerBanner')) : ?>
			<jdoc:include type="modules" name="headerBanner" style="custom" />
		<?php endif;?>
		<div class="<?php print (!$frontpage && $view === "article") ? "innerwidth" : ((!$frontpage && $view === "category") ? "innerwidth content--container" : " "); ?>">
			<div class="row-fluid">
			
				<?php if ($this->countModules('left')) : ?>
					<div class="span3 left">
						<jdoc:include type="modules" name="left" style="xhtml" />
					</div>				
				<?php endif; ?>
				
				<main id="content" role="main" class="span<?php echo $span; ?>">
				<!-- Begin Content -->
				<jdoc:include type="modules" name="position-3" style="xhtml" />
				<jdoc:include type="message" />
				<jdoc:include type="component" />
				<jdoc:include type="modules" name="position-2" style="none" />
				<!-- End Content -->
				</main>
				
				<?php if ($this->countModules('right')) : ?>
					<div class="span3 right">		
						<jdoc:include type="modules" name="right" style="xhtml" />
					</div>
				<?php endif; ?>
				
			</div>
		</div>
	</div>
</div>