<?php
/**
 * @author   	089webdesgin.de
 * @copyright   Copyright (C) 2015 089webdesgin.de. All rights reserved.
 * @URL 		https://089webdesgin.de/
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>
<footer class="footer fullwidth" role="contentinfo">
	<div class="footer-wrap">				
	<div class="vcard clr text-center">
		<img src="/images/kottermair-logo-small.png" alt="Elektroteam Kottermair Logo" />
			<p class="fn">Kottermair Elektroteam GmbH</p>
			<p class="adr"><span class="street-address">Salzstrasse 32 A</span> | <span class="postal-code">82110 </span><span class="region">Germering</span></p>
			<p class="tel "><a class="" href="tel:+49898419551">Tel: 089 - 841 9551</a></p>
	</div>								
	</div>
	<div id="copyright" class="fullwidth">
		<div class="copyWrapper ">
			<p>&copy; <?php print date("Y");?> Elektroteam Kottermair | <a class="imprLink" href="/impressum-kottermair-elektroteam-gmbh.html" title="Impressum FIRMA">Impressum</a> | <a class="imprLink" href="/datenschutz.html">Datenschutz</a></p>
		</div>
	</div>	
</footer>