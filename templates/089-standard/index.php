<?php
/**
 * @author   	cg@089webdesign.de
 */

defined('_JEXEC') or die;
//include system
include_once(JPATH_ROOT . "/templates/" . $this->template . '/lib/system.php');
//include template Functions CG
include_once(JPATH_ROOT . "/templates/" . $this->template . '/template_functions.php');
?>
<!DOCTYPE html>
<html lang="de-de">
<head>
	<?php //CG: weitere Fonts via prefetch oder preload HIER einfügen ?>
		<link rel="prefetch" as="font" crossorigin="crossorigin" type="font/ttf" href="/templates/089-standard/fonts/assistant-regular-webfont.ttf">
		<link rel="prefetch" as="font" crossorigin="crossorigin" type="font/ttf" href="/templates/089-standard/fonts/abel-regular-webfont.ttf">
	<?php 
	// including head
	include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/head.php');
	?>
</head>

<body id="body" class="site <?php print $detectAgent . ($clientMobile ? "mobile " : " ") . $body_class . ($layout ? $layout." " : '') . $option. ' view-' . $view. ($itemid ? ' itemid-' . $itemid : '') . $pageclass; ?>">

	<!-- Body -->
		<div class="fullwidth site_wrapper">
			<?php			
			
			// including header
			include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/header.php');						

			// including breadcrumb
			include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/breadcrumbs.php');		
									
			// including content
			include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/content.php');	
			
			// including bottom
			include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/bottom.php');	
			
			// including bottom2
			include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/bottom2.php');
			
			// including footer
			include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/footer.php');				
			
			?>					
			
		</div>
	
	
	<jdoc:include type="modules" name="debug" style="none" />

	<script type="text/javascript">
		window.onload = function () {
			setTimeout(function(){
				jQuery('body').addClass('ready');
			}, 500 ); 
		}

		jQuery(document).ready(function() {
			jQuery(window).scroll(function() {
					(jQuery(this).scrollTop() > 100 ) ? jQuery('#header').addClass('sticky') : jQuery('#header').removeClass('sticky') && jQuery('.innerwidth.content').css('margin-top', '0px');
			});
			<?php if($clientMobile) : ?>
				<?php //tablet menu open/close ?>
				jQuery('.btn-navbar').click("on", function() {
					jQuery('.nav-collapse.collapse').toggleClass('openMenu');
					jQuery('button.btn-navbar').toggleClass('btn-modify');
				});
			<?php endif; ?>			

		});		

	</script>
<?php if(!$clientMobile) : ?>
	<div id="resizeAlarm">
		<p>Das Fenster Ihres Webbrowsers ist zu klein - bitte vergrössern Sie ihr Browser-Fenster um die Inhalte sinnvoll darstellen zu können.</p>
	</div>
<?php endif; ?>	
</body>
</html>
